
import os
import os.path as path

import sys
import argparse

import bisect


def search_subdirs_for_duplicates(input_dir, output_file, verbosity) :
	if verbosity is not None: print("search_dir_for_duplicates() -- Entering...")

	# The following dictionary maps the files' relative paths with 
	# the (possibly multiple) subdirectories they've been found in:
	#
	full_base_path = path.abspath(input_dir)
	file_relPath_dirsFound_map = {}

	print("Py_DupeFiles_Finder:  Building map of files -> directories...")

	for current_dir, subdirs, _files in os.walk(full_base_path, topdown=True) :
		for current_subdir in subdirs:
			if verbosity is not None: print("search_dir_for_duplicates() -- Examining subdirectory <{0}>...".format(current_subdir) )
			current_subdir_full_path = path.join(full_base_path, current_subdir)

			for sub_sub_dir_walk_path, _sub_sub_dirs, files in os.walk(current_subdir_full_path, topdown=True) :

				for current_file in files:
					full_file_path = path.join(sub_sub_dir_walk_path, current_file)
					relative__path = None

					if full_file_path.startswith(current_subdir_full_path) :
						prefix_length  = len(current_subdir_full_path)
						relative__path = full_file_path[prefix_length:]
					else:
						exception_msg_template = "ERROR: full_file_path <{0}> doesn't start with full_base_path <{1}>!"
						full_exception_message = exception_msg_template.format(full_file_path, full_base_path)
						raise Exception(full_exception_message)
					#
					# END if full_file_path.startswith(full_base_path) : ... else: ...

					dirsFound_map_keys = file_relPath_dirsFound_map.keys()
					if relative__path not in dirsFound_map_keys:
						new_relPath_dirsFound_list = []
						new_relPath_dirsFound_list.append(current_subdir)

						file_relPath_dirsFound_map[relative__path] = new_relPath_dirsFound_list
					else:
						existing_relPath_dirsFound_list = file_relPath_dirsFound_map[relative__path]
						bisect.insort(existing_relPath_dirsFound_list, current_subdir)

						file_relPath_dirsFound_map[relative__path] = existing_relPath_dirsFound_list
					#
					# END if relative__path not in dirsFound_map_keys: ... else: ...

				#
				# END for current_file in files: ...

			#
			# END for current_dir, subdirs, files in os.walk(input_dir, topdown=True) : ...

		#
		# END for current_subdir in subdirs: ...

		# Clear out the "subdirs" list, because we don't actually *want*
		# to descend the directory tree in this (outer) loop:
		#
		subdirs[:] = []
	#
	# END for current_dir, subdirs, files in os.walk(input_dir, topdown=True) : ...

	# Re-process the file_relPath_dirsFound_map to create another dictionary,
	# that maps from a list of *places* that files were found onto a list of
	# *files* that were found in those places:
	#
	print("Py_DupeFiles_Finder:  Inverting map from files -> directories to directories -> files...")
	dirsFound_fileList_map = {}

	for foundFile in file_relPath_dirsFound_map.keys() :
		if verbosity is not None and verbosity >= 2: print("search_dir_for_duplicates() -- Examining file <{0}>...".format(foundFile) )

		dirsWhereFileWasFound = file_relPath_dirsFound_map[foundFile]
		dirsFound_sorted = tuple(sorted(dirsWhereFileWasFound))

		dirsFound_fileList_keys = dirsFound_fileList_map.keys()

		if dirsFound_sorted not in dirsFound_fileList_keys:
			new_dirsFound_fileList = []
			new_dirsFound_fileList.append(foundFile)

			dirsFound_fileList_map[dirsFound_sorted] = new_dirsFound_fileList
		else:
			existing_dirsFound_fileList = dirsFound_fileList_map[dirsFound_sorted]
			bisect.insort(existing_dirsFound_fileList, foundFile)

			dirsFound_fileList_map[dirsFound_sorted] = existing_dirsFound_fileList
		#
		# END if dirsFound_sorted not in dirsFound_fileList_keys: ... else: ...

	#
	# END for foundFile in file_relPath_dirsFound_map.keys() : ...

	# Now, finally, put together a report file, identifying
	# which files were present in which subdirectories:
	#
	print("Py_DupeFiles_Finder:  Generating report file...")

	final__dirsFound_fileList_keys = dirsFound_fileList_map.keys()
	sorted_dirsFound_fileList_keys = sorted(final__dirsFound_fileList_keys,
											key=lambda e: (-len(e), e) )

	total_file_sets  = len(sorted_dirsFound_fileList_keys)
	current_file_set = 1

	with output_file:
		output_file.write("Py_DupeFiles_Finder report:")
		output_file.write("\n")

		for dirList in sorted_dirsFound_fileList_keys:
			header_string = "Set #{0} of {1}:".format(current_file_set, total_file_sets)
			output_file.write(header_string)
			output_file.write("\n")

			dirs_string = "Found in: "
			first_dir   = True
			for current_dir in dirList:
				if not first_dir: dirs_string += ", "
				first_dir = False
				dirs_string += ("<" + current_dir + ">")
			#
			# END for current_dir in dirList: ...

			output_file.write(dirs_string)
			output_file.write("\n")

			files_found = dirsFound_fileList_map[dirList]
			for current_file in files_found:
				output_file.write(current_file)
				output_file.write("\n")
			#
			# END for current_dir in dirList: ...

			output_file.write("\n")
			output_file.write("\n")

			current_file_set += 1
		#
		# END for dirList in sorted_dirsFound_fileList_keys: ...

		output_file.write("End of Py_DupeFiles_Finder report.")
	#
	# END with output_file: ...

	return
#
# END search_subdirs_for_duplicates(input_dir, output_file, verbosity) : ...


if __name__ == "__main__":

	parser = argparse.ArgumentParser(	prog="Py_DupeFiles_Finder",
										description="Searches a series of subfolders for duplicate files.")

	parser.add_argument("directory", help="The directory whose subfolders you want to search for duplicate files",
						default=".")

	parser.add_argument("-v", "--verbosity", help="increase output verbosity",
						action="count")

	parser.add_argument("-o", "--output_file", type=argparse.FileType('w'))

	args = parser.parse_args()

	print("Py_DupeFiles_Finder:  Initializing...")

	search_subdirs_for_duplicates(args.directory, args.output_file, args.verbosity)

	print("Py_DupeFiles_Finder:  All done!  :-)")
#
# END if __name__ == "__main__": ...

